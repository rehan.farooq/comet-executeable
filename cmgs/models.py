from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=150)
    profession = models.CharField(max_length=200, null=True)
    age = models.IntegerField()
    email = models.CharField(max_length=250)
