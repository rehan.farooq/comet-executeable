from django.shortcuts import redirect
from django.views.generic import ListView

from .forms import PersonForm
from django.views.generic.edit import FormView

from .models import Person
from rest_framework import viewsets

from .serializers import PersonSerializer


class PersonView(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer

#
# class PersonFormView(FormView):
#     template_name = 'cmgs/person.html'
#     form_class = PersonForm
#
#     def post(self, request, *args, **kwargs):
#         data = request.POST
#         person = Person()
#         person.name = data['name']
#         person.profession = data['profession']
#         person.age = data['age']
#         person.email = data['email']
#         person.save()
#         return redirect('/')
#
#
# class PersonView(ListView):
#     model = Person
#     queryset = Person.objects.all()
#     template_name = 'cmgs/index.html'
#     context_object_name = 'persons'
