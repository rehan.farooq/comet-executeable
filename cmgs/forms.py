from django.forms import ModelForm

from cmgs.models import Person


class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = '__all__'

