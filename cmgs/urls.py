from django.urls import path, include
from .views import PersonView
from rest_framework import routers

router = routers.DefaultRouter()
router.register('', PersonView)

urlpatterns = [
    path('', include(router.urls)),
]
